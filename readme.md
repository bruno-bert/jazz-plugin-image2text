# Jazz Plugin: Image2Text Documentation

**If you want to be a contributor, [write to us](mailto:jazzcoreteam@gmail.com)**
Also, access the [Jazz Documentation website](https://jazz-docs.netlify.com) for more information.

## How it Works

#### Tasks:

- single task: Image2TextTask

#### Steps:

- receives an array of paths to images in the pipeline or as an input parameter
- converts each image to text and saves into an array of objects (key is image name)
- saves a log into an array of objects (key is image name)

## Plugin Pipeline

### Tasks

Contains a single task, named _image2text_.

#### image2text

**Purpose**: _image2text_ task is responsable for receiving an array of images and convert into an array of texts

**Class**: Image2TextTask

**Parameters**:

```javascript
export const pipeline = [
  {
    id: "image2text",
    description:
      "Receives an array of images and converts into an array of texts",
    class: "Image2TextTask",
    params: ["images"]
  }
];
```

### Input Parameters

```javascript
export const inputParameters = {
  images: {
    alias: "images",
    describe: "Array of Images",
    demandOption: true,
    default: [],
    name: "images"
  }
};
```

- images: indicates an array of images to be processed. If not informed, task wil try to get from pipeline

### Outputs

- single object with:
  - array of texts related to each image processed
  - array with log of each image processed with status indicator (processed sucessfully or failed)
