const gulp = require("gulp");
const { series } = require("gulp");
const jsonminify = require("gulp-jsonminify");
const del = require("del");
const uglify = require("gulp-uglify");

const buildFolder = "build";
const distFolder = "dist";
const jsonSources = "./src/**/*.json";
const jsSources = [`./${buildFolder}/**/*.js`];
const distReplaceFolder = `./${buildFolder}/__dist-replace__/**/*.js`;

const cleanBuild = () => del([`${buildFolder}/**/*`]);
const cleanDist = () => del([`${distFolder}/**/*`]);

const excludeFromDist = [
  `!${buildFolder}/**/__*/`,
  `!${buildFolder}/**/__*/**/*`
];

const jsSourceToUglify = [...jsSources, ...excludeFromDist];

const minifyJson = async () =>
  gulp
    .src(jsonSources)
    .pipe(jsonminify())
    .pipe(gulp.dest(buildFolder));

const minifyJsonDist = async () =>
  gulp
    .src([jsonSources, "!./src/**/__*/", "!./src/**/__*/**"])
    .pipe(jsonminify())
    .pipe(gulp.dest(distFolder));

const uglifyJs = () =>
  gulp
    .src(jsSourceToUglify)
    .pipe(uglify())
    .pipe(gulp.dest(distFolder));

const replaceDist = async () =>
  gulp
    .src(distReplaceFolder)
    .pipe(uglify())
    .pipe(gulp.dest(distFolder, { overwrite: true }));

gulp.task("cleanBuild", cleanBuild);
gulp.task("cleanDist", cleanDist);
gulp.task("minifyJson", minifyJson);
gulp.task("minifyJsonDist", minifyJsonDist);
gulp.task("replaceDist", replaceDist);

gulp.task("dist", series(cleanDist, minifyJsonDist, uglifyJs, replaceDist));
