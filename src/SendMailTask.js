/* eslint-disable class-methods-use-this */
import Task from "./Task";
import { name as pluginName } from "./config";
import { sendMail } from "./helpers";

class SendMailTask extends Task {
  constructor(id, params, config, description = "", rawDataFrom = null) {
    super(id, params, config, description, rawDataFrom);

    [this.plugin] = this.config.plugins.filter(
      plugin => plugin.name === pluginName
    );

    /** add here the task parameters */
    this.mailTo = params.mailTo;

    this.validateParameters({
      ...params,
      mailConnection: this.plugin.tasks.sendmail.mailConnection
    });
  }

  execute(data) {
    this.rawData = data || this.getRawData();
    const mailConnection = this.plugin.tasks.sendmail.mailConnection;
    const outputFilePropertyName = this.plugin.tasks.sendmail
      .outputFilePropertyName;

    if (!this.validateConditionsForExecution()) return;

    const mailOptions = {
      from: mailConnection.mailFrom, // sender address
      to: this.mailTo, // list of receivers
      subject: "Hello Jazz", // Subject line
      text: "your mail body here", // plain text body
      html: "<b>your mail body here</b>" // html body,
    };

    /* add attachments */
    if (this.rawData[outputFilePropertyName]) {
      const attachments = [
        {
          path: this.rawData[outputFilePropertyName]
        }
      ];

      mailOptions.attachments = attachments;
    }

    sendMail(mailConnection, mailOptions, (err, info) => {
      if (!err) {
        this.onSuccess(info);
      } else {
        this.onError("Error when trying to send email: " + err);
      }
    });
  }

  onSuccess(info) {
    super.onSuccess(info);

    this.logger.success(
      `${String(this.constructor.name)} - Email sent successfully`
    );
  }

  validateParameters(params) {
    /** add here parameters validation */
    if (!params.mailTo) {
      this.onError("Mail To is required");
    }

    if (!params.mailConnection) {
      this.onError("Mail Connection information is required");
    }
  }
}
module.exports = SendMailTask;
