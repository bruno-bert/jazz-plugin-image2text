const source = "this";
const defaultRelativePluginPath = "../..";
const textLoaderPluginPath = `${defaultRelativePluginPath}/jazz-plugin-textloader/dist`;

module.exports = {
  pipeline: [
    `${textLoaderPluginPath}:jazz-plugin-textloader:load`,
    `${source}:jazz-plugin-sendmail:sendmail`
  ],
  plugins: [
    {
      name: "jazz-plugin-textloader",
      tasks: {
        load: {
          getRawData: () => ["test1", "test2", "test3"]
        }
      }
    },
    {
      name: "jazz-plugin-sendmail",
      tasks: {
        sendmail: {
          rawDataFrom: "load",
          outputFilePropertyName: "outputFile",
          mailConnection: {
            mailFrom: "bruno.bert.jj@gmail.com",
            service: "gmail",
            host: "smtp.gmail.com",
            user: "bruno.bert.jj@gmail.com",
            password: "icwsig89"
          }
        }
      }
    }
  ]
};
