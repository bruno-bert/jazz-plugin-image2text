export const name = "jazz-plugin-sendmail";
export const pipeline = [
  {
    id: "sendmail",
    description: "Send information from pipeline to email",
    class: "SendMailTask",
    params: ["mailTo"]
  }
];

export const inputParameters = {
  mailTo: {
    alias: "mail",
    describe: "Mail To",
    demandOption: true,
    default: "teste",
    name: "mailTo"
  }
};
