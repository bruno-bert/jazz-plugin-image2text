const nodemailer = require("nodemailer");

const sendMail = async (mailConnection, mailOptions, cb) => {
  // create reusable transporter object using the default SMTP transport

  let transporter = nodemailer.createTransport({
    service: mailConnection.service,
    host: mailConnection.host,
    auth: {
      user: mailConnection.user,
      pass: mailConnection.password
    }
  });

  // send mail with defined transport object
  await transporter.sendMail(mailOptions, (err, info) => {
    cb(err, info);
  });
};

module.exports = sendMail;
